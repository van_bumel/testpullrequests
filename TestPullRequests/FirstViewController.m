//
//  ViewController.m
//  TestPullRequests
//
//  Created by Chaban Nikolay on 10/2/17.
//  Copyright © 2017 IvanChaban. All rights reserved.
//

#import "FirstViewController.h"

@interface FirstViewController ()

//Properties
@property (weak, nonatomic) IBOutlet UIButton* showNextControllerButton;

//Methods
- (IBAction) onShowNextController: (UIButton*) sender;

@end

@implementation FirstViewController


#pragma mark - Life cycle -

- (void) viewDidLoad
{
    [super viewDidLoad];
}


#pragma mark - Memory managment -

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - Actions -

- (IBAction) onShowNextController: (UIButton*) sender
{
    
}
@end
