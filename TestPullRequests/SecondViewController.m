//
//  SecondViewController.m
//  TestPullRequests
//
//  Created by Chaban Nikolay on 10/2/17.
//  Copyright © 2017 IvanChaban. All rights reserved.
//

#import "SecondViewController.h"

@interface SecondViewController ()

//Properties

//Methods

@end

@implementation SecondViewController


#pragma mark - Life cycle -

- (void) viewDidLoad
{
    [super viewDidLoad];
}


#pragma mark - Memory managment -

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


@end
