//
//  AppDelegate.h
//  TestPullRequests
//
//  Created by Chaban Nikolay on 10/2/17.
//  Copyright © 2017 IvanChaban. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

